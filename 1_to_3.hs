-- Functions
-- Infix functions
someDiv = div 92 10
someOtherDiv = 92 `div` 10

-- Prefix functions
doubleMe x = x + x
doubleUs x y = 2*x + 2*y
doubleTheSmallNumber x y = if x < y
                            then 2*x
                            else 2*y
doubleTheSmallNumber2 x y = 2 * (min x y)

-- Strings
-- Sting are lists
someString = "This is string"
someOtherString = "and this is another string"
newString = someString ++ someOtherString

someArray = [1, 2, 3, 4, 5]
someOtherArray = [11, 22, 33, 44, 55]
newArray = someArray ++ someOtherArray
-- Note:
--  1. `++` operator takes two lists as params
--  2. `:` operator takes one number (or char) and one list as params
someString2 = 'A':" small cat" -- 'A' is `char`; " small cat" is `list`
someArray2 = 1:[2,3,4,5,6]
someArray3 = 1:2:3:[]
firstIndiceOfList = [9.4,33.2,96.2,11.2,23.25] !! 1

-- List compare
lc1 = [3, 2, 1] > [2, 1, 0]
lc2 = [3, 2, 1] > [2, 10, 100]
lc3 = [3, 4, 2] > [3, 4]
lc4 =[3, 4, 2] > [2, 4]
lc5 = [3, 4, 2] == [3, 4, 2]
someList = [5, 4, 3, 2, 1]
lf1 = head someList -- 5
lf2 = tail someList -- [4, 3, 2, 1]
lf3 = last someList -- 1
lf4 = init someList -- [5, 4, 3, 2]

-- List comprehension
someMath = [x * 2 | x <- [1..10]]
someMath2 = [x * 2 | x <- [1..10], x * 2 >= 12]  -- add some constrain
someMath3 = [ x | x <- [50..100], x `mod` 7 == 3]
boomBangs xs = [ if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x]
someBang = boomBangs [7..13] -- ["BOOM!","BOOM!","BANG!","BANG!"]
someMath4 = [ x*y | x <- [2,5,10], y <- [8,10,11], x*y > 50]
someMath5 xs ys = [ x*y | x <- xs, y <- ys, x*y > 50]

nouns = ["hobo","frog","pope"]
adjectives = ["lazy","grouchy","scheming"]
someWords = [ adj ++ " " ++ noun | adj <- adjectives, noun <- nouns ]
someWords2 = [ adj ++ " " ++ noun | noun <- nouns, adj <- adjectives ]
length' xs = sum [1 | _ <- xs]

matrixA = replicate 5 (replicate 5 1)
triangles = [ (a,b,c) | c <- [1..10], b <- [1..c], a <- [1..b] ]
rightTriangles = [ (a,b,c) | c <- [1..10], b <- [1..c], a <- [1..b], a^2 + b^2 == c^2 ]

doubleIfOdd x =
  if mod x 2 == 1
    then x * 2
    else x
