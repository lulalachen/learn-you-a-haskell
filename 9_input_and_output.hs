module Main where

-- Compile it by
-- > ghc 9_input_and_output.hs
main = do
  putStrLn "Hello World"
  name <- getLine
  putStrLn ("Hello, " ++ name)

