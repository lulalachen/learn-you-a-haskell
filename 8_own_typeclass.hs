import Data.Char
import Data.List
import Geometry.Shapes
import qualified Data.Map as Map

main = do
  printAllWithTitle
    "1-1"
    [
      surface $ Rectangle (Point 0 0) (Point 5 5)
    , surface $ Circle (Point 0 0) 10
    ]
  printAllWithTitle
    "1-2"
    [
      nudge (Rectangle (Point 0 0) (Point 5 5)) 5 10
    ]
  printAllWithTitle
    "2 - Records"
    [
      firstName guy
    , lastName guy
    ]
  printAllWithTitle
    "3.1 - DataTypes"
    [
      enum1
    , enum2
    ]
  printAllWithTitle
    "3.2 - DataTypes (Ord)"
    [
      eqOrd1
    ]
  printAllWithTitle
    "3.3 - DataTypes (Bool)"
    [
      eqOrd2
    ]
  printAllWithTitle
    "3.4 - DataTypes (Ord)"
    [
      bounded1
    , bounded2
    ]

printAllWithTitle :: (Show a) => String -> [a] -> IO ()
printAllWithTitle title xs = do
  putStrLn title
  putStrLn "------"
  mapM_ print xs
  putStrLn "========="


----------
-- Records
----------

-- data Person = Person String String Int Float String String deriving (Show)
data Person = Person { firstName :: String
                     , lastName :: String
                     , age :: Int
                     } deriving (Show, Read, Eq)

data Car = Car { company :: String
               , model :: String
               , year :: Int
               } deriving (Show, Eq)

guy = Person { firstName = "lulala", lastName = "chen", age = 24 }

-- data Vector a = Vector a a a deriving (Show)

-- vplus :: (Num t) => Vector t -> Vector t -> Vector t
-- (Vector i j k) `vplus` (Vector l m n) = Vector (i+l) (j+m) (k+n)

-- vectMult :: (Num t) => Vector t -> t -> Vector t
-- (Vector i j k) `vectMult` m = Vector (i*m) (j*m) (k*m)

-- scalarMult :: (Num t) => Vector t -> Vector t -> t
-- (Vector i j k) `scalarMult` (Vector l m n) = i*l + j*m + k*n


data Day = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday
           deriving (Eq, Ord, Show, Read, Bounded, Enum)

-- Eq, Ord
eqOrd1 = compare Monday Sunday  -- LT
eqOrd2 = Monday > Sunday  -- False

-- Show, Read

-- Bounded
bounded1 = minBound :: Day  -- Monday
bounded2 = maxBound :: Day  -- Sunday

-- Enum
enum1 = [Monday .. Sunday]  -- [Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday]
enum2 = [minBound .. maxBound]  -- [Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday]


