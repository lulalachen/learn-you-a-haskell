module Geometry.Shapes
( Point(..)
, Shape(..)
, surface
, nudge
, baseCircle
, baseRect
) where

-- data Shape = Circle Float Float Float | Rectangle Float Float Float Float deriving (Show)
-- It's better written like below
data Point
  = Point Float Float deriving (Show)
data Shape
  = Circle Point Float
  | Rectangle Point Point
  deriving (Show)


surface :: Shape -> Float
surface (Circle _ r) = pi * r * r

surface (Rectangle (Point x1 y1) (Point x2 y2))
  = (abs $ x1 - x2) * (abs $ y1 - y2)


nudge :: Shape -> Float -> Float -> Shape
nudge
  (Circle (Point x y) r)
  dx
  dy
  = Circle (Point x' y') r
  where x' = x + dx
        y' = y + dy

nudge
  (Rectangle (Point x1 y1) (Point x2 y2))
  dx
  dy
  = Rectangle (Point x1' y1') (Point x2' y2')
  where x1' = x1 + dx
        y1' = y1 + dy
        x2' = x2 + dx
        y2' = y2 + dy


baseCircle :: Float -> Shape
baseCircle = Circle (Point 0 0)

baseRect :: Float -> Float -> Shape
baseRect width height = Rectangle (Point 0 0) (Point width height)
