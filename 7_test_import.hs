import qualified Geometry.Sphere as Sphere

main = do
  print $ Sphere.area 5
  print $ Sphere.volume 5
