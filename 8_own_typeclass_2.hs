import qualified Data.Map as Map

main =
    printAllWithTitle
        "1 LookupMap"
        [
            lockerLookup 100 lockers
        ,   lockerLookup 101 lockers
        ,   lockerLookup 102 lockers
        ]        

printAllWithTitle :: (Show a) => String -> [a] -> IO ()
printAllWithTitle title xs = do
  putStrLn title
  putStrLn "------"
  mapM_ print xs
  putStrLn "========="

-- Either example: LockerState
-- data Either a b = Left a | Right b deriving (Eq, Ord, Read, Show)
data LockerState = Taken | Free deriving (Show, Eq)
type Code = String
type LockerMap = Map.Map Int (LockerState, Code)

lockerLookup :: Int -> LockerMap -> Either String Code
lockerLookup lockerNumber lockerMap =
    case Map.lookup lockerNumber lockerMap of
        Nothing -> Left $ "Locker number " ++ show lockerNumber ++ " doesn't exist!"
        Just (state, code) -> if state /= Taken
                                then Right code
                                else Left $ "Locker " ++ show lockerNumber ++ " is already taken!"

lockers :: LockerMap
lockers = Map.fromList
    [(100,(Taken,"ZD39I"))
    ,(101,(Free,"JAH3I"))
    ,(103,(Free,"IQSA9"))
    ,(105,(Free,"QOTSA"))
    ,(109,(Taken,"893JJ"))
    ,(110,(Taken,"99292"))
    ]

-- data List a = Empty | Cons { listHead :: a, listTail :: List a} deriving (Show, Read, Eq, Ord)
-- data List a = Empty | Cons a (List a) deriving (Show, Read, Eq, Ord)
-- Infix define
infixr 5 :-:
data List a = Empty | a :-: (List a) deriving (Show, Read, Eq, Ord)

-- infixr 5  ++
-- (++) :: [a] -> [a] -> [a]
-- []     ++ ys = ys
-- (x:xs) ++ ys = x : (xs ++ ys)

-- infixr 5  .++
-- (.++) :: List a -> List a -> List a
-- Empty .++ ys = ys
-- (x :-: xs) .++ ys = x :-: (xs .++ ys)
