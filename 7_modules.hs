import Data.Char
import Data.Map
import Data.List as List

------------
-- Data.List
------------

-- intersperse
intersperse '.' "MONKEY"  -- "M.O.N.K.E.Y"

-- intercalate
intercalate " " ["hey","there","guys"]  -- "hey there guys"

-- transpose
transpose [ [1,2,3], [4,5,6], [7,8,9] ]

-- and, or, any, all

-- iterate
show $ take 10 $ iterate (*2) 1
show $ take 3 $ iterate (++ "haha") "haha"

-- splitAt
show $ splitAt 3 "heyman"  -- (hey, man)

-- takeWhile
-- dropWhile
list =  [1,2,2,2,3,4,5,4,3,2,1]
show $ takeWhile (<3) list  -- [1,2,2,2]
show $ dropWhile (<3) list  -- [3,4,5,4,3,2,1]

-- break, span
list2 = [1,2,3,4,5,6,7]
show $ break (== 4) list2  -- ([1,2,3], [4,5,6,7])
show $ span (\= 4) list2  -- ([1,2,3], [4,5,6,7])

-- inits, tails
str = "abcde"
show $ inits str  -- ["","a","ab","abc","abcd","abcde"]
show $ tails str  -- ["abcde","bcde","cde","de","e",""]

-- isInfixOf, isPrefixOf
show $ isInfixOf "cat" "I have a cat"  -- True
show $ isInfixOf "Cat" "I have a cat"  -- False
show $ isPrefixOf "cat" "cats are cute"  -- True
show $ isPrefixOf "cat" "I have a cat"  -- False

-- partition :: (a -> Bool) -> [a] -> ([a], [a])
-- takes predicate and list seperate into two lists by predicate
show $ partition (`elem` ['A'..'Z']) "BOBsidneyMORGANeddy"  -- ("BOBMORGAN","sidneyeddy")
show $ span (`elem` ['A'..'Z']) "BOBsidneyMORGANeddy"  -- ("BOB","sidneyMORGANeddy")

-- find :: Foldable t => (a -> Bool) -> t a -> Maybe a
show $ find (<4) [5,2,3,6,1]  -- Just 2
show $ find (==4) [1,2,3]  -- Nothing

find' :: (a -> Bool) -> [a] -> Maybe a
find' f [] = Nothing
find' f (x:xs) = if f x == True
                    then Just x
                    else find' f xs

-- elemIndex, elemIndices
show $ 4 `elemIndex` [1,2,3,4,5,6]  -- Just 3
show $ 10 `elemIndex` [1,2,3,4,5,6]  -- Nothing

-- findIndex, findIndices
findIndex (==4) [5,3,2,1,6,4]  -- Just 5
findIndex (==7) [5,3,2,1,6,4]  -- Nothing

-- zip, zip3, ...; zipWith, zipWith3, ...;
 ,
-- lines, unlines
show $ lines "first line\nsecond line\nthird line"  -- ["first line","second line","third line"]
show $ unlines ["first line", "second line", "third line"]  -- "first line\nsecond line\nthird line\n"

-- words, unwords
words "hey these    are    the words in this\nsentence"  -- ["hey","these","are","the","words","in","this","sentence"]
unwords ["hey","there","mate"]  -- "hey there mate"

-- nub
-- remove duplicates


-- delete
-- delete the first element in list
delete 'h' "hello hey yo"

-- //
"Im a big baby" \\ "big"
"big big cat" \\ (sort "biggib")  -- "  cat"

-- union, intersect

-- nubBy, deleteBy, unionBy, intersectBy and groupBy
groupBy ((==) `on` (> 0)) values
-- [[-4.3,-2.4,-1.2],[0.4,2.3,5.9,10.5,29.1,5.3],[-2.4,-14.5],[2.9,2.3]]

let xs = [[5,4,5,4,4],[1,2,3],[3,5,4,3],[],[2],[2,2]]
sortBy (compare `on` length) xs
-- [[],[2],[2,2],[1,2,3],[3,5,4,3],[5,4,5,4,4]]


------------
-- Data.Char
------------

-- generalCategory :: (Char a, GeneralCategory b) => a -> b
generalCategory 'A'  -- UppercaseLetter

-- ord, chr
ord 'a'  -- 97
chr 100  -- c

encode :: Int -> String -> String
encode shift msg =
  let ords = map ord msg
      shifted = map (+shift) ords
  in  map chr shifted

decode :: Int -> String -> String
decode shift msg = encode (negate shift) msg


--------------
-- Data.Map
--------------

findKey :: (Eq k) => k -> [(k,v)] -> Maybe v
findKey key [] = Nothing
findKey key ((k,v):xs) = if key == k
                            then Just v
                            else findKey key xs

findKey' :: (Eq k) => k -> [(k,v)] -> Maybe v
findKey' key = Data.List.foldl
              (
               \acc (k, v) ->
               if k == key
                then Just v
                else acc
              )
              Nothing

-- fromList, toList
phonebook = [("betty","555-2938"),("bonnie","452-2928"),("lucille","205-2928")]
m = fromList phonebook  -- fromList [("betty","555-2938"),("bonnie","452-2928"),("lucille","205-2928")]
Map.toList m  -- [("betty","555-2938"),("bonnie","452-2928"),("lucille","205-2928")]

-- lookup, member
Map.lookup :: Ord k => k -> Map.Map k a -> Maybe a
Map.lookup "lulala" m
Map.member :: Ord k => k -> Map.Map k a -> Bool
Map.member "lulala" m

-- map, filter
Map.map (*10) $ Map.fromList [(1,1),(2,4),(3,9)]
Map.filter isUpper $ Map.fromList [(1,'a'),(2,'A'),(3,'b'),(4,'B')]

-- keys, elems

phoneBook =
  [("betty","555-2938")
  ,("betty","342-2492")
  ,("bonnie","452-2928")
  ,("patsy","493-2928")
  ,("patsy","943-2929")
  ,("patsy","827-9162")
  ,("lucille","205-2928")
  ,("wendy","939-8282")
  ,("penny","853-2492")
  ,("penny","555-2111")
  ]

m = Map.fromListWith (++) $ List.map (\(k, v) -> (k, [v])) phoneBook

Map.lookup "patsy" $ phoneBookToMap phoneBook  -- ["827-9162","943-2929","493-2928"]


-----------
-- Data.Set
-----------

text1 = "I just had an anime dream. Anime... Reality... Are they so different?"
text2 = "The old man left his garbage can out and now his trash is all over my lawn!"

s1 = Set.fromList text1
s2 = Set.fromList text2

-- intersection, difference, union, null, size, member, empty, singleton, insert and delete

-- isSubSetOf, isProperSubsetOf

-- map, filter
