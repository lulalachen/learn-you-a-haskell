multThree :: (Num a) => a -> a -> a -> a
multThree x y z = x * y * z
multTwoWithNine = multThree 9
-- multTwoWithNine 2 3
multWithEighteen = multTwoWithNine 2
-- multWithEighteen 10

compareWithHundred :: (Num a, Ord a) => a -> Ordering
compareWithHundred x = compare 100 x
-- Notice that the x is on the right hand side on both sides of the equation
-- Now let's think about what compare 100 returns.
-- It returns a function that takes a number and compares it with 100

compareWithHundred' :: (Num a, Ord a) => a -> Ordering
compareWithHundred' = compare 100

applyTwice :: (a -> a) -> a -> a
applyTwice f x = f (f x)

-- use three different type
zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ [] _ = []
zipWith' _ _ [] = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys

largestDivisible :: (Integral a) => a
largestDivisible = head (filter p [100000,99999..])
  where p x = x `mod` 3829 == 0

quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) =
  smallerPart ++ [x] ++ largerPart
  where smallerPart = filter (< x) xs
        largerPart  = filter (>= x) xs

chain :: (Integral a) => a -> [a]
chain 1 = [1]
chain n
  | even n = n:chain (n `div` 2)
  | odd n = n:chain (n * 3 + 1)

oddSquareSum :: Integer
oddSquareSum =
  let oddSquares = filter odd $ map (^2) [1..]
      belowLimit = takeWhile (<10000) oddSquares
  in sum belowLimit
